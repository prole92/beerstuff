from django.contrib import admin
from beer.models import BeerType, Beer
# Register your models here.
admin.site.register(BeerType)
admin.site.register(Beer)
