from django.db import models

# Create your models here.
class BeerType(models.Model):
    """
    Class that defines type of the beer
    """
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=5000)

    def __str__(self):
        return self.name

class Beer(models.Model):
    """
    Class that defines beer
    """
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=5000)
    beer_type = models.ForeignKey(BeerType, related_name='beers', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
