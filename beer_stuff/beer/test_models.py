from django.test import TestCase
from beer.models import BeerType, Beer

class BeerTypeTestCase(TestCase):
    def setUp(self):
        ale_description = "Ale is a type of beer brewed using a warm "+\
                          "fermentation method, resulting in a sweet, "+\
                          "full-bodied and fruity taste. Historically,"+\
                          "the term referred to a drink brewed without hops."
        self.beer_ale = BeerType.objects.create(name="Ale",
                                                description=ale_description)

    def test_string(self):
        self.assertEqual(str(self.beer_ale), "Ale")

class BeerTestCase(TestCase):
    def setUp(self):
        self.ale_description = "Ale is a type of beer brewed using a warm "+\
                          "fermentation method, resulting in a sweet, "+\
                          "full-bodied and fruity taste. Historically,"+\
                          "the term referred to a drink brewed without hops."
        self.beer_ale = BeerType.objects.create(name="Ale",
                                                description=self.ale_description)
        self.nektar_description = "Nektar pivo odlikuje savršena svježina "+\
                                  "i pitkost koja nastaje spajanjem tri "+\
                                  "posebne vrste hmelja i kristalno čiste "+\
                                  "planinske vode, recept koji se poštuje "+\
                                  "kroz generacije još od davne 1873. godine."
        self.beer = Beer.objects.create(name="Nektar",
                                        description=self.nektar_description,
                                        beer_type=self.beer_ale)

    def test_beer_to_string(self):
        """
        Tests __str__ method of Beer class
        """
        self.assertEqual(str(self.beer), self.beer.name)
