from beer.models import BeerType, Beer
from django.test import TestCase
from django.core.urlresolvers import reverse

class BeerTypeViewTests(TestCase):
    """
    Test class used for testing views for Beer Type
    """
    def setUp(self):
        ale_description = "Ale is a type of beer brewed using a warm "+\
                          "fermentation method, resulting in a sweet, "+\
                          "full-bodied and fruity taste. Historically,"+\
                          "the term referred to a drink brewed without hops."
        self.beer_ale = BeerType.objects.create(name="Ale",
                                                description=ale_description)

    def test_beer_type_detail(self):
        response = self.client.get(reverse('beer:beertypedetail',
                                           kwargs={'pk':self.beer_ale.id}))
        self.assertContains(response, "<b>Name:</b> Ale")

    def test_beer_type_list(self):
        response = self.client.get(reverse('beer:beer_type_list'))
        self.assertContains(response, "Ale")

class BeerViewTests(TestCase):
    def setUp(self):
        self.ale_description = "Ale is a type of beer brewed using a warm "+\
                          "fermentation method, resulting in a sweet, "+\
                          "full-bodied and fruity taste. Historically,"+\
                          "the term referred to a drink brewed without hops."
        self.beer_ale = BeerType.objects.create(name="Ale",
                                                description=self.ale_description)
        self.nektar_description = "Nektar pivo odlikuje savršena svježina "+\
                                  "i pitkost koja nastaje spajanjem tri "+\
                                  "posebne vrste hmelja i kristalno čiste "+\
                                  "planinske vode, recept koji se poštuje "+\
                                  "kroz generacije još od davne 1873. godine."
        self.beer = Beer.objects.create(name="Nektar",
                                        description=self.nektar_description,
                                        beer_type=self.beer_ale)

    def test_beer_detail(self):
        response = self.client.get(reverse('beer:detail',
                                           kwargs={'pk':self.beer.id}))
        self.assertContains(response, 'Nektar')

    def test_beer_list(self):
        response = self.client.get(reverse('beer:list'))
        self.assertContains(response, 'Nektar')
