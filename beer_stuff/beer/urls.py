from django.conf.urls import url
from beer import views

app_name = 'beer'

urlpatterns = [
    url(r'^$', views.BeerListView.as_view(), name='list'),
    url(r'^types/$', views.BeerTypeListView.as_view(), name='beer_type_list'),
    url(r'^types/(?P<pk>[-\w]+)/$', views.BeerTypeDetailView.as_view(), name='beertypedetail'),
    url(r'^(?P<pk>[-\w]+)/$', views.BeerDetailView.as_view(), name='detail'),
]
