from django.views.generic import ListView, DetailView
from . import models
# Create your views here.

class BeerTypeListView(ListView):
    model = models.BeerType

class BeerTypeDetailView(DetailView):
    model = models.BeerType
    template_name = 'beer/beer_type_detail.html'

class BeerListView(ListView):
    model = models.Beer

class BeerDetailView(DetailView):
    model = models.Beer
    template_name = 'beer/beer_detail.html'
