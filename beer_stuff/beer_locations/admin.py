from django.contrib import admin
from beer_locations.models import BeerLocation
# Register your models here.
admin.site.register(BeerLocation)
