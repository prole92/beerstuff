from django.apps import AppConfig


class BeerLocationsConfig(AppConfig):
    name = 'beer_locations'
