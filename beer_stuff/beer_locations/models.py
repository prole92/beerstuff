from django.db import models
from beer.models import Beer
from locations.models import Location
# Create your models here.

class BeerLocation(models.Model):
    beer = models.OneToOneField(Beer, on_delete=models.CASCADE)
    location = models.OneToOneField(Location, on_delete=models.CASCADE)
    price = models.FloatField()

    def __str__(self):
        return self.beer.name + " in " + self.location.name
