from django.test import TestCase
from beer_locations.models import BeerLocation
from beer.models import Beer, BeerType
from locations.models import Location, LocationType
# Create your tests here.

class BeerLocationTestCases(TestCase):
    def setUp(self):
        self.location_type = LocationType.objects.create(name="Pub",
                                                         description="Pub description")
        self.location = Location.objects.create(name="Mac Tire",
                                                description='A great pub',
                                                photo='bla/bla',
                                                coordinate_x=11.111,
                                                coordinate_y=22.222,
                                                coordinate_z=33.333,
                                                location_type=self.location_type)
        self.ale_description = "Ale is a type of beer brewed using a warm "+\
                          "fermentation method, resulting in a sweet, "+\
                          "full-bodied and fruity taste. Historically,"+\
                          "the term referred to a drink brewed without hops."
        self.beer_ale = BeerType.objects.create(name="Ale",
                                                description=self.ale_description)
        self.nektar_description = "Nektar pivo odlikuje savršena svježina "+\
                                  "i pitkost koja nastaje spajanjem tri "+\
                                  "posebne vrste hmelja i kristalno čiste "+\
                                  "planinske vode, recept koji se poštuje "+\
                                  "kroz generacije još od davne 1873. godine."
        self.beer = Beer.objects.create(name="Nektar",
                                        description=self.nektar_description,
                                        beer_type=self.beer_ale)
        self.beer_location = BeerLocation.objects.create(beer=self.beer,
                                                         location=self.location,
                                                         price=3.0)

    def test_beer_location_to_string(self):
        expected_beer_location_name = self.beer.name + " in " + self.location.name
        self.assertEqual(str(self.beer_location), expected_beer_location_name)
