from django.conf.urls import url
from beer_locations import views

app_name = 'beer_locations'

urlpatterns = [
    url(r'^$', views.BeerLocationsListView.as_view(), name='list')
]
