# Create your views here.
from django.views.generic import ListView, DetailView
from . import models

class BeerLocationsListView(ListView):
    model = models.BeerLocation

class BeerLocationDetailView(DetailView):
    model = models.BeerLocation
