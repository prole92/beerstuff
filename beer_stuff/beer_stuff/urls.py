"""beer_stuff URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from beercount import views as beercount_views
from beer import views as beer_views
from locations import views as location_views
from beer_locations import views as beer_locations_views

urlpatterns = [
    url(r'^$', beercount_views.index, name='index'),
    url(r'^admin/', admin.site.urls),
    url(r'^beercount/', include('beercount.urls')),
    url(r'^locations/', include('locations.urls')),
    url(r'^beer-locations/', include('beer_locations.urls')),
    url(r'^ratings/', include('ratings.urls')),
    url(r'^logout/$', beercount_views.user_logout, name='logout'),
    url(r'^beer/',include('beer.urls'), name='beer'),
]
