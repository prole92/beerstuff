from django.apps import AppConfig


class BeercountConfig(AppConfig):
    name = 'beercount'
