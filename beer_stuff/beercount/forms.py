import datetime
from django import forms
from django.contrib.auth.models import User
from beercount.models import BeerCount

BIRTH_YEAR_CHOICES = [str(year) for year in range(1940, 2000)]
class UserForm(forms.ModelForm):
    """
    Form used for user registration
    """
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = User
        fields = ('username', 'email', 'password')

    def clean_email(self):
        """
        Method used to verify if email is alrady in use, before the user
        can register
        """
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if email and User.objects.filter(email=email).exclude(username=username).exists():
            raise forms.ValidationError(u'Email addresses must be unique.')
        return email

class BeerCountForm(forms.ModelForm):
    """
    Form used to gather additional user info such ase date_of_birth
    """
    date_of_birth = forms.DateField(
        widget=forms.SelectDateWidget(years=BIRTH_YEAR_CHOICES),
        initial=datetime.date(year=1992, month=8, day=20))
    class Meta():
        model = BeerCount
        fields = ('date_of_birth',)

    def clean_date_of_birth(self):
        """
        Method that validates if the user that is attempting to registeri
        is old enough to do so
        """
        date_of_birth = self.cleaned_data['date_of_birth']
        beercount = BeerCount(date_of_birth=date_of_birth)
        if not beercount.is_old_enough_to_register():
            raise forms.ValidationError(u'You need to be older than 18 in order to register.')
        return date_of_birth
