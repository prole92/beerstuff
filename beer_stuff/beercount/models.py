from datetime import date
from django.db import models
from django.contrib.auth.models import User

class BeerCount(models.Model):
    """
    Class that will be used as a single sign on account for multiple sites
    Related to beer
    """
    user = models.OneToOneField(User)
    date_of_birth = models.DateField()

    def calculate_age(self):
        """
        Calculates the age of the BeerCount owner
        """
        today = date.today()
        return today.year - self.date_of_birth.year - \
                ((today.month,
                  today.day) < \
                (self.date_of_birth.month,
                 self.date_of_birth.day))

    def is_old_enough_to_register(self):
        """
        Returns true if the owner of the BeerCount is more than 18 years older
        Which means they are old enough to register
        """
        return self.calculate_age() >= 18

    def __str__(self):
        return self.user.username
