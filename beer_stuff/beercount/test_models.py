import datetime
from beercount.models import BeerCount
from django.test import TestCase
from django.contrib.auth.models import User
from freezegun import freeze_time
# Create your tests here.

@freeze_time("2017-10-29")
class BeerCountTestCase(TestCase):

    def setUp(self):
        user = User.objects.create(first_name='Firstname',
                                   last_name='Lastname',
                                   username='username')
        beercount = BeerCount.objects.create(user=user,
                                             date_of_birth=datetime.date(
                                                 year=1992,
                                                 month=8,
                                                 day=20))

        user = User.objects.create(first_name='Firstname',
                                   last_name='Lastname',
                                   username='minor')
        beercount = BeerCount.objects.create(user=user,
                                             date_of_birth=datetime.date(
                                                 year=2001,
                                                 month=8,
                                                 day=20))
        beercount.user = user
        beercount.save()

    def test_beercount_created(self):
        """
        Used to verify if the BeerCount is successfully created with
        all valid data
        """
        firstname_beercount = BeerCount.objects.get(user__username='username')

        self.assertEqual(firstname_beercount.user.first_name, "Firstname")
        self.assertEqual(firstname_beercount.user.last_name, "Lastname")
        self.assertEqual(firstname_beercount.user.username, "username")
        self.assertEqual(firstname_beercount.date_of_birth,
                         datetime.date(
                             year=1992,
                             month=8,
                             day=20))

    def test_calculate_age(self):
        """
        Used to verify that calculation of the users age is working
        """
        firstname_beercount = BeerCount.objects.get(user__username='username')

        self.assertEqual(firstname_beercount.calculate_age(), 25)

    def test_is_old_enough_to_register(self):
        """
        Used to verify that the method that validates if the user is old enough
        to register is working properly when the user is actually old enough
        to register
        """
        firstname_beercount = BeerCount.objects.get(user__username='username')

        self.assertEqual(firstname_beercount.is_old_enough_to_register(), True)

    def test_is_old_enough_to_reg_neg(self):
        """
        Used to verify that the method that validates if the user is old enough
        to register is working properly when the user is not old enough to
        register
        """
        firstname_beercount = BeerCount.objects.get(user__username='minor')

        self.assertEqual(firstname_beercount.is_old_enough_to_register(), False)

    def test_beercount_str(self):
        """
        Used to verify that the BeerCount string representation is the
        username of the BeerCount
        """
        firstname_beercount = BeerCount.objects.get(user__username='username')
        self.assertEqual(str(firstname_beercount), 'username')
