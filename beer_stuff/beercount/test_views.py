import datetime
from beercount.models import BeerCount
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

#TODO Consider mocking forms?

class BeerCountViewTests(TestCase):
    """
    Test class used for testing of BeerCount registration form in both
    valid and invalid scenarios (positive and negative tests)
    """
    def setUp(self):
        self.credentials_login = {
            'username': 'test_login',
            'password': 'password'
        }
        self.incorrect_credentials = {
            'username': 'test_login',
            'password': 'password2'
        }
        super(BeerCountViewTests, self).setUp()
        User.objects.create(username='exists', email='exists@exists.com')
        User.objects.create_user(**self.credentials_login)

    def test_get_index(self):
        """
        BeerCount Index test method
        """
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'BeerD')

    def test_get_register(self):
        """
        BeerCount register get test. Used to verify if all fields that need
        to be displayed are actually displayed in the response
        """
        response = self.client.get(reverse('beercount:register'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'BeerD')
        self.assertContains(response, 'Username')
        self.assertContains(response, 'Email address')
        self.assertContains(response, 'Password')
        self.assertContains(response, 'Date of birth')

    def test_post_reg_invalid_date(self):
        """
        Test method used to verify server side validation of the date
        """
        data = {
            'username':'username',
            'first_name':'first_name',
            'email':'emai@lemail.com',
            'password':'password',
            'date_of_birth':'date_of_birth'
        }
        response = self.client.post(reverse('beercount:register'), data)
        self.assertContains(response, 'Enter a valid date.')

    def test_post_reg_invalid_email(self):
        """
        Test method used to verify server side validation of the email
        """
        data = {
            'username':'username',
            'first_name':'email',
            'email':'emailcom',
            'password':'password',
            'date_of_birth_month':'8',
            'date_of_birth_day':'20',
            'date_of_birth_year':'1992',
        }
        response = self.client.post(reverse('beercount:register'), data)
        self.assertContains(response, 'Enter a valid email address.')

    def test_post_register(self):
        """
        Test method used to verify that posting to the register form creates
        the user successfully
        """
        data = {
            'username':'username2',
            'email':'emai@lemail.com',
            'password':'password',
            'date_of_birth_month':'8',
            'date_of_birth_day':'20',
            'date_of_birth_year':'1992',
        }
        response = self.client.post(reverse('beercount:register'), data)
        self.assertContains(response, '<h1>Thank you for registering!</h1>')
        self.assertEqual(response.status_code, 200)
        firstname_beercount = BeerCount.objects.get(user__username='username2')

        self.assertEqual(firstname_beercount.date_of_birth,
                         datetime.date(year=1992, month=8, day=20))
        self.assertEqual(firstname_beercount.user.username, data['username'])
        self.assertEqual(firstname_beercount.user.email, data['email'])

    def test_post_reg_username_exists(self):
        """
        Test method used to verify that error message will be shown in case
        the user with the attepmted username is already registered
        """
        user_exists = User.objects.get(username='exists')
        self.assertEqual(user_exists.username, 'exists')
        data = {
            'username':'exists',
            'email':'emai@lemail.com',
            'password':'password',
            'date_of_birth_month':'8',
            'date_of_birth_day':'20',
            'date_of_birth_year':'1992',
        }

        response = self.client.post(reverse('beercount:register'), data)
        self.assertContains(response,
                            'A user with that username already exists.')

    def test_post_reg_email_exists(self):
        """
        Test method used to verify that error message will be shown in case
        the user with the attepmted email is already registered
        """
        email_exists = User.objects.get(email='exists@exists.com')
        self.assertEqual(email_exists.email, 'exists@exists.com')
        data = {
            'username':'bla',
            'email':'exists@exists.com',
            'password':'password',
            'date_of_birth_month':'8',
            'date_of_birth_day':'20',
            'date_of_birth_year':'1992',
        }

        response = self.client.post(reverse('beercount:register'), data)
        self.assertContains(response, 'Email addresses must be unique.')

    def test_post_reg_older_than_18(self):
        """
        Test method used to make sure that validation of the users age
        is working. User should not be able to register if they are less than
        18 years old.
        """
        data = {
            'username':'username3',
            'email':'emai3@lemail.com',
            'password':'password',
            'date_of_birth_month':'8',
            'date_of_birth_day':'20',
            'date_of_birth_year':'2000',
        }
        response = self.client.post(reverse('beercount:register'), data)
        self.assertContains(response,
                            'You need to be older than 18 in order to register.')
        self.assertEqual(response.status_code, 200)
        #User.objects.get(username='username3')
        #'User matching query does not exist.'
        with self.assertRaises(Exception) as context:
            User.objects.get(username='username3')
        print(context.exception)
        self.assertTrue(str(context.exception) == 'User matching query does not exist.')

    def test_login_get(self):
        """
        Test method that verifies that initial page that is shown to the user
        when they try to login is correct
        """
        response = self.client.get(reverse('beercount:user_login'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Login')
        self.assertContains(response, 'input type="text" name="username"')
        self.assertContains(response, 'input type="password" name="password"')
        self.assertContains(response, 'input type="submit"')
        self.assertContains(response, 'value="Login"')

    def test_login_success_post(self):
        """
        Test method for testing if login is successfull when user is
        registered and authenticated
        """
        response = self.client.post(reverse('beercount:user_login'),
                                    self.credentials_login,
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Logout')

    def test_login_failed_post(self):
        """
        Test method for testing if login is not successfull when
        user is not registered and authenticated
        """
        response = self.client.post(reverse('beercount:user_login'),
                                    self.incorrect_credentials,
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Invalid login details supplied!')

    def test_login_deactivated_account(self):
        """
        Test method for testing if login is not successfull when
        user is not registered and authenticated
        """

        deactivated_user = User.objects.get(username='test_login')
        deactivated_user.is_active = False
        deactivated_user.save()
        response = self.client.post(reverse('beercount:user_login'),
                                    self.credentials_login,
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        print(response)
        self.assertContains(response, 'Invalid login details supplied!')

    def test_logout(self):
        """
        Test method that checks if user is able to successfully logout
        """
        response = self.client.post(reverse('beercount:user_login'),
                                    self.credentials_login,
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Logout')
        response = self.client.post(reverse('logout'),
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Sign In')
