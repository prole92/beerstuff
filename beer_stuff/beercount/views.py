from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from beercount.forms import UserForm, BeerCountForm

# Create your views here.

def index(request):
    """
    Index view. Home page
    """
    return render(request, 'index.html')

@login_required
def user_logout(request):
    """
    User logout view
    """
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def register(request):
    """
    Registration view
    """
    registered = False

    if request.method == "POST":
        user_form = UserForm(data=request.POST)
        profile_form = BeerCountForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():

            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()

            registered = True
        else:
            print(user_form.errors, profile_form.errors)
    else:
        user_form = UserForm()
        profile_form = BeerCountForm()

    return render(request, 'beercount/register.html', {
        'registered': registered,
        'user_form':user_form,
        'profile_form':profile_form,
    })

def user_login(request):
    """
    Login view used to authenticate the user
    """
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('index'))

        #if user didn't pass the authentication return statement will be
        #skipped and message for invalid credentials should be returned
        #TODO: Figure out logging
        print("Someone tried to login and failed!")
        print("Username: {} , Password: {}".format(username, password))
        return HttpResponse("Invalid login details supplied!")
    else:
        return render(request, 'beercount/login.html', {})
