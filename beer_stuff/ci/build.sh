#!/usr/bin/env bash

BUILD_ROOT="beer_stuff/"
MNGR=${BUILD_ROOT}"manage.py"
# Install requirements
source ./ve/bin/activate
python ${MNGR} makemigrations
python ${MNGR} migrate
python ${MNGR} jenkins --enable-coverage --pylint-load-plugins pylint_django beercount beer locations beer_locations ratings
deactivate
