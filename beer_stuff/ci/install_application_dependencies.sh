#!/usr/bin/env bash
BUILD_ROOT="beer_stuff/"
source ./ve/bin/activate
pip install -r ${BUILD_ROOT}requirements.txt
deactivate
