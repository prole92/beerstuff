from django.contrib import admin
from locations.models import LocationType, Location
# Register your models here.

admin.site.register(LocationType)
admin.site.register(Location)
