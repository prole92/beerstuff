from django.db import models

# Create your models here.
class LocationType(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=5000)

    def __str__(self):
        return self.name

class Location(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=5000)
    photo = models.CharField(max_length=1000)
    coordinate_x = models.FloatField()
    coordinate_y = models.FloatField()
    coordinate_z = models.FloatField()
    location_type = models.ForeignKey(LocationType,
                                      related_name='locations',
                                      on_delete=models.CASCADE)

    def __str__(self):
        return self.name
