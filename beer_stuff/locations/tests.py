from django.test import TestCase
from locations.models import LocationType, Location
# Create your tests here.
class LocationTypeTestCases(TestCase):
    def setUp(self):
        self.location_type = LocationType.objects.create(name="Pub",
                                                         description="Pub description")

    def test_location_type_to_string(self):
        self.assertEqual(str(self.location_type), self.location_type.name)

class LocationTestCases(TestCase):
    def setUp(self):
        self.location_type = LocationType.objects.create(name="Pub",
                                                         description="Pub description")
        self.location = Location.objects.create(name="Mac Tire",
                                                description='A great pub',
                                                photo='bla/bla',
                                                coordinate_x=11.111,
                                                coordinate_y=22.222,
                                                coordinate_z=33.333,
                                                location_type=self.location_type)

    def test_location_to_string(self):
        self.assertEqual(str(self.location), self.location.name)
