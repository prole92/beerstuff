from django.conf.urls import url
from . import views

app_name = 'locations'

urlpatterns = [
    url(r'^types/$', views.LocationTypeListView.as_view(), name='location_type_list'),
    url(r'^types/(?P<pk>[-\w]+)/$',
        views.LocationTypeDetailView.as_view(),
        name='location_type_detail'),
    url(r'^$', views.LocationListView.as_view(), name='location_list'),
    url(r'^(?P<pk>[-\w]+)/$', views.LocationDetailView.as_view(), name='location_detail'),
]
