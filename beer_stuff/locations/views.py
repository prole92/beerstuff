from django.views.generic import ListView, DetailView
from . import models
# Create your views here.

class LocationTypeListView(ListView):
    model = models.LocationType

class LocationTypeDetailView(DetailView):
    model = models.LocationType

class LocationListView(ListView):
    model = models.Location

class LocationDetailView(DetailView):
    model = models.Location
