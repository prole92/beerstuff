from django.contrib import admin
from ratings.models import Rating
from beercount.models import BeerCount
# Register your models here.

class RatingAdmin(admin.ModelAdmin):
    fields = ('title', 'description', 'rating_numeric', 'beer_location')
    def save_model(self, request, obj, form, change):
        print('bla')
        instance = form.save(commit=False)
        if not hasattr(instance, 'beercount'):
            print('bla2')
            instance.beercount = BeerCount.objects.get(user_id=request.user.id)
        instance.save()
        form.save_m2m()
        return instance

admin.site.register(Rating, RatingAdmin)
