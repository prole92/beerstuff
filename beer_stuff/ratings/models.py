from datetime import datetime
from django.db import models
from django.core.urlresolvers import reverse
from beercount.models import BeerCount
from beer_locations.models import BeerLocation

# Create your models here.
class Rating(models.Model):
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=5000)
    rating_numeric = models.FloatField()
    beercount = models.ForeignKey(BeerCount,
                                  related_name='beercount',
                                  on_delete=models.CASCADE)

    datetime_of_rating = models.DateTimeField(default=datetime.now, blank=True)
    beer_location = models.ForeignKey(BeerLocation,
                                      related_name='beer_location',
                                      on_delete=models.CASCADE)


    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('ratings:rating_detail', kwargs={'pk':self.pk})
