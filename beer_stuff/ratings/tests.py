import datetime
from django.test import TestCase
from django.contrib.auth.models import User
from beer.models import Beer, BeerType
from beer_locations.models import BeerLocation
from ratings.models import Rating
from locations.models import Location, LocationType
from beercount.models import BeerCount
# Create your tests here.

class RatingTestCases(TestCase):
    def setUp(self):
        self.location_type = LocationType.objects.create(name="Pub",
                                                         description="Pub description")
        self.location = Location.objects.create(name="Mac Tire",
                                                description='A great pub',
                                                photo='bla/bla',
                                                coordinate_x=11.111,
                                                coordinate_y=22.222,
                                                coordinate_z=33.333,
                                                location_type=self.location_type)
        self.ale_description = "Ale is a type of beer brewed using a warm "+\
                          "fermentation method, resulting in a sweet, "+\
                          "full-bodied and fruity taste. Historically,"+\
                          "the term referred to a drink brewed without hops."
        self.beer_ale = BeerType.objects.create(name="Ale",
                                                description=self.ale_description)
        self.nektar_description = "Nektar pivo odlikuje savršena svježina "+\
                                  "i pitkost koja nastaje spajanjem tri "+\
                                  "posebne vrste hmelja i kristalno čiste "+\
                                  "planinske vode, recept koji se poštuje "+\
                                  "kroz generacije još od davne 1873. godine."
        self.beer = Beer.objects.create(name="Nektar",
                                        description=self.nektar_description,
                                        beer_type=self.beer_ale)
        self.beer_location = BeerLocation.objects.create(beer=self.beer,
                                                         location=self.location,
                                                         price=3.0)
        self.user = user = User.objects.create(first_name='Firstname',
                                               last_name='Lastname',
                                               username='username')
        self.beercount = BeerCount.objects.create(user=user,
                                                  date_of_birth=datetime.date(
                                                      year=1992,
                                                      month=8,
                                                      day=20))
        self.rating = Rating.objects.create(title="new rating",
                                            description="Description of the new rating",
                                            rating_numeric=10,
                                            beercount=self.beercount,
                                            datetime_of_rating=datetime.datetime.now(),
                                            beer_location=self.beer_location)

    def test_rating_to_string(self):
        self.assertEqual(str(self.rating), self.rating.title)
