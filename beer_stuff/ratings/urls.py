from django.conf.urls import url
from . import views

app_name = 'ratings'

urlpatterns = [
    url(r'^$', views.RatingListView.as_view(), name="rating_list"),
    url(r'^add/$', views.RatingCreate.as_view(), name='rating_create'),
    url(r'^(?P<pk>[-\w]+)/$', views.RatingDetailView.as_view(), name='rating_detail'),
]
