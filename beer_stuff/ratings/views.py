# Create your views here.
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from beercount.models import BeerCount
from . import models

class RatingListView(ListView):
    model = models.Rating

class RatingDetailView(DetailView):
    model = models.Rating

@method_decorator(login_required, name='dispatch')
class RatingCreate(CreateView):
    model = models.Rating
    fields = ['title', 'description', 'rating_numeric', 'beer_location']
    template_name = 'ratings/rating_add.html'

    def form_valid(self, form):
        print(self.request.user.id)
        form.instance.beercount = BeerCount.objects.get(user__id=self.request.user.id)
        return super(RatingCreate, self).form_valid(form)
